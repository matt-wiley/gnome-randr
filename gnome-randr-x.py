#!/bin/env python3

import sys, os, json
from collections import defaultdict

from argsy import Argsy

# from stackoverflow.com/questions/5369723
nested_dict = lambda: defaultdict(nested_dict)


# usage: gnome-randr.py [options]
#         where options are:
#         --current
#         --dry-run
#         --persistent
#         --global-scale <global-scale>
#         --output <output>
#                 --auto
#                 --mode <mode>
#                 --rate <rate>
#                 --scale <scale>
#                 --off
#                 --right-of <output>
#                 --left-of <output>
#                 --above <output>
#                 --below <output>
#                 --same-as <output>
#                 --rotate normal,inverted,left,right
#                 --primary

ARG_DEF="""
program:
  name: gnome-randr-x.py
  description: A command line tool for setting up monitors in Gnome.
  subcommands:
    cmds:
      output:
        help: "Output options."
        args:
          auto:
            cmd_type: option
            flags: "-a|--auto"
            help: "Auto."
            action: store_true
          mode:
            cmd_type: option
            flags: "-m|--mode"
            help: "Mode."
            type: str
          rate:
            cmd_type: option
            flags: "-r|--rate"
            help: "Rate."
            type: str
          scale:
            cmd_type: option
            flags: "-s|--scale"
            help: "Scale."
            type: str
          off:
            cmd_type: option
            flags: "-O|--off"
            help: "Off."
            action: store_true
          right-of:
            cmd_type: option
            flags: "-R|--right-of"
            help: "Right of."
            type: str
          left-of:
            cmd_type: option
            flags: "-L|--left-of"
            help: "Left of."
            type: str
          above:
            cmd_type: option
            flags: "-A|--above"
            help: "Above."
            type: str
          below:
            cmd_type: option
            flags: "-B|--below"
            help: "Below."
            type: str
          same-as:
            cmd_type: option
            flags: "-S|--same-as"
            help: "Same as."
            type: str
          rotate:
            cmd_type: option
            flags: "-Q|--rotate"
            help: "Rotate."
            type: str
          primary:
            cmd_type: option
            flags: "-p|--primary"
            help: "Primary."
            action: store_true
  args:
    current:
      cmd_type: option
      flags: "-c|--current"
      help: "Use current configuration."
      action: store_true
    dry_run:
      cmd_type: option
      flags: "-d|--dry-run"
      help: "Dry run."
      action: store_true
    persistent:
      cmd_type: option
      flags: "-p|--persistent"
      help: "Make persistent."
      action: store_true
    global_scale:
      cmd_type: option
      flags: "-g|--global-scale"
      help: "Global scale."
      type: float
"""


def main():
  parsed_args = Argsy(config_str=ARG_DEF).parse_args(sys.argv[1:])
  json.dump(parsed_args, sys.stdout, indent=2)


if __name__ == "__main__":
  main()
